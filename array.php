<!DOCTYPE html>
<html>
<head>
	<title>Array</title>
	<link rel="stylesheet" type="text/css" href="ta9.css">
</head>
<body>
	<div>
		<div class="header">
			<h1>WEB SEDERHANA</h1>
		</div>
		<div class="navigation">
			<ul>
				<li><a href="ta9.php">Home</a></li>
				<li><a href="php1.php">PHP-#1</a></li>
				<li><a href="php2percabangan.php">PHP-#2 Percabangan</a></li>
				<li><a href="php2perulangan.php">PHP-#2 Perulangan</a></li>
				<li><a href="array.php">PHP-#4 Array</a></li>
				<li><a href="fungsarray.php">PHP-#4 Fungsi Array</a></li>
			</ul>
		</div>
		<div class="badan">
			<div align="center" class="sidebar">
				<font size="5">
					<p>Sosial media</p>
					<a href="https://www.twitter.com/banjalancali/">Twitter</a>
					<a href="https://www.instagram.com/peekartboo_/">Instagram</a>
					<a href="https://web.facebook.com/anjaarrr/">Facebook</a>
				</font>
			</div>
			<div class="content">
				<font size="5">
					<?php
						$arrWarna = array("Red", "Orange", "Yellow", "Green", "Blue", "Purpple" );
						echo "Menampilkan isi array dengan FOR :<br>";
						for ($i=0; $i < count($arrWarna) ; $i++) { 
							# code...
							echo "Warna Pelangi <font color = $arrWarna[$i]>". $arrWarna[$i]."</font><br>";
						}

						echo "<br>Menampilkan isi array dengan FOREACH :<br>";
						foreach ($arrWarna as $warna) {
							# code...
							echo "Warna Pelangi <font color = $warna>". $warna."</font><br>";
						}
					 ?>
				</font>
			</div>
		</div>
		<div align="center" class="footer">&copy Muhammad Banjaransari</div>
	</div>
</body>
</html>